<?php namespace MusicTest\Console\Commands;

use Illuminate\Console\Command;
use MusicTest\Models\Album;
use MusicTest\Models\Auth;
use MusicTest\Models\Track;
use Symfony\Component\Console\Input\InputArgument;

class AddAuthAccount extends Command
{

    protected $albums = [
        [
            'title'  => 'The A-Team: Original Motion Picture Soundtrack',
            'ASIN'   => 'unknown',
            'tracks' => [
                ['name' => "Somewhere in Mexico", 'length' => 132],
                ['name' => "Saving Face", 'length' => 132],
                ['name' => "Alpha Mike Foxtrot", 'length' => 132],
                ['name' => "Welcome to Baghdad", 'length' => 132],
                ['name' => "The Plan", 'length' => 132],
                ['name' => "Court Martial", 'length' => 132],
                ['name' => "Putting the Team Back Together", 'length' => 132],
                ['name' => "Flying a Tank", 'length' => 132],
                ['name' => "Frankfurt", 'length' => 132],
                ['name' => "Retrieving the Plates", 'length' => 132],
                ['name' => "Safehouse", 'length' => 132],
                ['name' => "Safehouse Aftermath", 'length' => 132],
                ['name' => "Shell Game", 'length' => 132],
                ['name' => "The Docks (Part 1)", 'length' => 132],
                ['name' => "The Docks (Part 2)", 'length' => 132],
                ['name' => "I Love It When a Plan Comes Together", 'length' => 132],
            ]
        ]
    ];
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'user:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add Auth Account';
    /**
     * @var Auth
     */
    private $auth;
    /**
     * @var Album
     */
    private $album;
    /**
     * @var Track
     */
    private $track;

    /**
     * Create a new command instance.
     *
     * @param Auth  $auth
     * @param Album $album
     * @param Track $track
     */
    public function __construct(Auth $auth, Album $album, Track $track)
    {
        parent::__construct();
        $this->auth = $auth;
        $this->album = $album;
        $this->track = $track;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $auth = $this->auth->create(['username' => $this->argument('username'), 'token' => md5(time())]);
        $this->line($auth->token);
        foreach ($this->albums as $album) {
            $albumRow = $this->album->create([
                'title'   => $album['title'],
                'auth_id' => $auth->getId()
            ]);
            foreach ($album['tracks'] as $track) {
                $trackRow = $this->track->create([
                    'title'    => $track['name'],
                    'length'   => $track['length'],
                    'auth_id'  => $auth->getId(),
                    'album_id' => $albumRow->id
                ]);
            }
        }

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['username', InputArgument::REQUIRED, 'User/Api Username'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }

}
