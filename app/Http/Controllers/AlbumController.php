<?php
namespace MusicTest\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use MusicTest\Http\Requests\CreateAlbumHttpRequest;
use MusicTest\Http\Requests\UpdateAlbumHttpRequest;
use MusicTest\Models\Album;
use MusicTest\Models\AuthInterface;

/**
 * Class AlbumController
 * @package MusicTest\Http\Controllers
 * @author  Simon Bennett <simon@bennett.im>
 */
final class AlbumController extends Controller
{
    /**
     * @var Album
     */
    private $album;
    /**
     * @var AuthInterface
     */
    private $authInterface;

    /**
     * @param Album         $album
     * @param AuthInterface $authInterface
     */
    public function __construct(Album $album, AuthInterface $authInterface)
    {
        $this->album = $album;
        $this->authInterface = $authInterface;
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        return new JsonResponse([
            'albums' => $this->album->with('tracks')->where('auth_id', $this->authInterface->getId())->get()
        ]);
    }

    /**
     * @param CreateAlbumHttpRequest $request
     * @return JsonResponse
     */
    public function store(CreateAlbumHttpRequest $request)
    {
        $album = $this->album->create([
            'title'   => $request->get('title'),
            'auth_id' => $this->authInterface->getId(),
        ]);

        return new JsonResponse(['success' => true, 'album' => $album]);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        try {
            return new JsonResponse([
                'album' => $this->album->with('tracks')->where('auth_id',
                    $this->authInterface->getId())->findOrFail($id)
            ]);
        } catch (ModelNotFoundException $ex) {
            return new JsonResponse(['error' => 'Album Not found'], 404);
        }
    }

    /**
     * @param UpdateAlbumHttpRequest $request
     * @param                        $id
     * @return JsonResponse
     */
    public function update(UpdateAlbumHttpRequest $request, $id)
    {
        try {
            /** @var Album $album */
            $album = $this->album->where('auth_id',
                $this->authInterface->getId())->findOrFail($id);
            $album->update([
                'title' => $request->get('title', $album->title),
            ]);

            return new JsonResponse(['success' => true, 'album' => $album]);

        } catch (ModelNotFoundException $ex) {
            return new JsonResponse(['error' => 'Album Not found'], 404);

        }
    }

    /**
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            /** @var Album $album */
            $album = $this->album->where('auth_id',
                $this->authInterface->getId())->findOrFail($id);
            $album->delete();

            return new JsonResponse(['success' => true]);

        } catch (ModelNotFoundException $ex) {
            return new JsonResponse(['error' => 'Album Not found'], 404);

        }
    }
}
