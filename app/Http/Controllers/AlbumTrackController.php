<?php
namespace MusicTest\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use MusicTest\Http\Requests\CreateAlbumTrackHttpRequest;
use MusicTest\Http\Requests\UpdateAlbumTrackHttpRequest;
use MusicTest\Models\Album;
use MusicTest\Models\AuthInterface;
use MusicTest\Models\Track;

/**
 * Class AlbumTrackController
 * @package MusicTest\Http\Controllers
 * @author  Simon Bennett <simon@bennett.im>
 */
final class AlbumTrackController extends Controller
{
    /**
     * @var Album
     */
    private $album;
    /**
     * @var Track
     */
    private $track;
    /**
     * @var AuthInterface
     */
    private $authInterface;

    /**
     * @param Album         $album
     * @param Track         $track
     * @param AuthInterface $authInterface
     */
    public function __construct(Album $album, Track $track, AuthInterface $authInterface)
    {
        $this->album = $album;
        $this->track = $track;
        $this->authInterface = $authInterface;
    }

    /**
     * @param $albumId
     * @return JsonResponse
     */
    public function index($albumId)
    {
        try {
            $this->album->findOrFail($albumId);

            return new JsonResponse([
                'tracks' => $this->track->where("album_id", $albumId)->where('auth_id',
                    $this->authInterface->getId())->get()
            ]);

        } catch (ModelNotFoundException $ex) {
            return new JsonResponse(['error' => 'Album Not found'], 404);
        }

    }

    /**
     * @param int                         $albumId
     * @param CreateAlbumTrackHttpRequest $request
     * @return JsonResponse
     */
    public function store($albumId, CreateAlbumTrackHttpRequest $request)
    {
        try {
            $this->album->findOrFail($albumId);

            $track = $this->track->create([
                'album_id' => $albumId,
                'length'   => $request->get('length'),
                'title'    => $request->get('title'),
                'auth_id'  => $this->authInterface->getId(),

            ]);

            return new JsonResponse(['success' => true, 'track' => $track]);

        } catch (ModelNotFoundException $ex) {
            return new JsonResponse(['error' => 'Album Not found'], 404);
        }
    }

    /**
     * @param int $albumId
     * @param int $trackId
     * @return JsonResponse
     */
    public function show($albumId, $trackId)
    {
        try {
            return new JsonResponse([
                'track' => $this->track->where("album_id", $albumId)->where('auth_id',
                    $this->authInterface->getId())->where('id', $trackId)->get()
            ]);

        } catch (ModelNotFoundException $ex) {
            return new JsonResponse(['error' => 'Track Not found'], 404);

        }
    }

    /**
     * @param int                         $albumId
     * @param int                         $trackId
     * @param UpdateAlbumTrackHttpRequest $request
     * @return JsonResponse
     */
    public function update($albumId, $trackId, UpdateAlbumTrackHttpRequest $request)
    {
        try {
            /** @var Track $track */
            $track = $this->track->where("album_id", $albumId)->where('auth_id',
                $this->authInterface->getId())->where('id', $trackId)->firstOrFail();

            $track->update([
                'title'  => $request->get('title', $track->title),
                'length' => (int)$request->get('length', $track->length)
            ]);

            return new JsonResponse(['success' => true, 'track' => $track]);

        } catch (ModelNotFoundException $ex) {
            return new JsonResponse(['error' => 'Track Not found'], 404);

        }
    }

    public function destroy($albumId, $trackId)
    {
        try {
            /** @var Track $track */
            $track = $this->track->where("album_id", $albumId)->where('id', $trackId)->where('auth_id',
                $this->authInterface->getId())->firstOrFail();

            $track->delete();

            return new JsonResponse(['success' => true]);

        } catch (ModelNotFoundException $ex) {
            return new JsonResponse(['error' => 'Track Not found'], 404);

        }
    }
}