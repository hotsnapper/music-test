<?php namespace MusicTest\Http\Controllers;

use Illuminate\Http\Request;
use MusicTest\Http\Parsedown;

class WelcomeController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */
    /**
     * @var Request
     */
    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function index()
    {
        return view('welcome');
    }

    public function docs()
    {
        return view('docs', ['docs' => $this->parseDocument(file_get_contents(__DIR__ . '/../docs.md'))]);
    }

    private function parseDocument($docs)
    {
        $docs = str_replace('{url}', $this->request->root(), $docs);
        $docs = str_replace('{docs}', $this->request->root() . '/admin/documentation', $docs);

        $docs = preg_replace("/{(embed):(.*)}/", "![Doc Image](/admin/documentation/image$2)", $docs);
        $parser = new Parsedown();

        return $parser->text($docs);
    }
}
