<?php namespace MusicTest\Http\Middleware;

use Closure;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use MusicTest\Models\Auth;
use MusicTest\Models\AuthInterface;

class APILoginMiddleware
{
    /**
     * @var Auth
     */
    private $auth;
    /**
     * @var Application
     */
    private $application;

    /**
     * @param Auth        $auth
     * @param Application $application
     */
    public function __construct(Auth $auth, Application $application)
    {
        $this->auth = $auth;
        $this->application = $application;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $auth = $this->auth->where("username", $request->headers->get('api_username'))->where('token',
                $request->headers->get('api_token'))->firstOrFail();
            \App::bind(AuthInterface::class, function ($app) use ($auth) {
                return $auth;
            });

        } catch (ModelNotFoundException $ex) {
            return new JsonResponse(['error' => 'Api Login not found (need valid api-username and api-token headers on each request)'],
                401);
        }

        return $next($request);
    }

}
