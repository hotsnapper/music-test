<?php namespace MusicTest\Http\Requests;

use MusicTest\Http\Requests\Request;

class UpdateAlbumTrackHttpRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        return [
            'title' => '',
            'length' => 'integer|min:0'
        ];
	}

}
