#Auth#

To authenticate with the app you will need to send the 'api-username' and 'api-token' that we provided you with in the header of every request.
This will give you your own private section to the system

The Api has been developed in a RESTful manner. Enabling you to predict the URLs and methods available.
Check for validation errors which will be sent back when creating and updating resources to check what is required


#Albums#

Albums must have a title

|url|method|description|
|---|-------|----------|
|{url}/albums|GET| Fetch all albums|
|{url}/albums|POST| Create New Album|
|{url}/albums/{albumid}|GET| Fetch one album by id|
|{url}/albums/{albumid}|PUT| Update Album|
|{url}/albums/{albumid}|DELETE| Delete Album|

#Albums#

Tracks must have a title and length (in seconds) 

|url|method|description|
|---|-------|----------|
|{url}/albums/{albumid}/tracks|GET| Fetch all albums Tracks|
|{url}/albums/{albumid}/tracks|POST| Create New Track|
|{url}/albums/{albumid}/tracks/{trackId}|GET| Fetch one Track by id|
|{url}/albums/{albumid}/tracks/{trackId}|PUT| Update Track|
|{url}/albums/{albumid}/tracks/{trackId}|DELETE| Delete Track|
