<?php
namespace MusicTest\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Album
 * @author  Simon Bennett <simon@bennett.im>
 */
final class Album extends Model
{
    protected $fillable = ['title', 'ASIN','auth_id'];
    protected $hidden = ['auth_id'];
    public function tracks()
    {
        return $this->hasMany('MusicTest\Models\Track');
    }
}