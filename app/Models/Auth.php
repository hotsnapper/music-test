<?php
namespace MusicTest\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Auth
 * @package MusicTest\Models
 * @author  Simon Bennett <simon@bennett.im>
 */
final class Auth extends Model implements AuthInterface
{
    protected $fillable = ['username', 'token'];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}