<?php
namespace MusicTest\Models;

/**
 * Interface AuthInterface
 * @package MusicTest\Models
 * @author  Simon Bennett <simon@bennett.im>
 */
interface AuthInterface
{
    /**
     * @return int
     */
   public function getId();
}