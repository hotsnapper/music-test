<?php
namespace MusicTest\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Track
 * @property mixed title
 * @package MusicTest\Models
 * @author  Simon Bennett <simon@bennett.im>
 */
final class Track extends Model
{
    protected $fillable = ['title', 'length','album_id','auth_id'];
    protected $hidden = ['auth_id'];

}