<html>
<head>
    <link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Source+Code+Pro' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
    <title>HotSnapper Test API</title>
    <style>
        body {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 200;
            font-family: 'Lato';
        }

        .container {
            display: table-cell;
            vertical-align: middle;
            padding: 10px;
            text-align: center;

        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 96px;
            margin-bottom: 40px;
        }

        .quote {
            font-size: 24px;
        }

        p {
            font-size: 16px;
        }

        .docs {
            color: #1e2123;
            text-align: left;
            font-family: 'Source Code Pro';
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title">Docs</div>

    </div>
    <div class="docs">
        <p>
            {!!$docs!!}

        </p>
    </div>
</div>
</body>
</html>
